package com.example.epidemics

import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.uber.simplestore.impl.SimpleStoreFactory
import okhttp3.OkHttpClient

class SymptomsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_symptoms_selector)
        setSupportActionBar(findViewById(R.id.toolbar))
        supportActionBar!!.title = "Voltar à tela inicial"
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val backend = BackendClient(this@SymptomsActivity, OkHttpClient(), "http://192.168.0.16:51051")
        val deviceId = intent!!.extras!!.getString("deviceId")!!

        val selectedOptions = mutableMapOf(
            "FEVER" to false,
            "DRY_COUGH" to false,
            "TIREDNESS" to false,
            "ACHES_AND_PAINS" to false,
            "SORE_THROAT" to false,
            "DIARRHOEA" to false,
            "CONJUNCTIVITIS" to false,
            "HEADACHE" to false,
            "LOSS_OF_TASTE_OR_SMELL" to false,
            "SKIN_RASHES" to false,
            "DISCOLORATION_OF_FINGERS" to false,
            "BREATH_DIFFICULTY" to false,
            "CHEST_PAIN" to false,
            "LOSS_OF_SPEECH" to false,
            "LOSS_OF_MOVEMENT" to false
        )

        val boxes = arrayOf(
            findViewById<CheckBox>(R.id.FEVER),
            findViewById<CheckBox>(R.id.DRY_COUGH),
            findViewById<CheckBox>(R.id.TIREDNESS),
            findViewById<CheckBox>(R.id.ACHES_AND_PAINS),
            findViewById<CheckBox>(R.id.SORE_THROAT),
            findViewById<CheckBox>(R.id.DIARRHOEA),
            findViewById<CheckBox>(R.id.CONJUNCTIVITIS),
            findViewById<CheckBox>(R.id.HEADACHE),
            findViewById<CheckBox>(R.id.LOSS_OF_TASTE_OR_SMELL),
            findViewById<CheckBox>(R.id.SKIN_RASHES),
            findViewById<CheckBox>(R.id.DISCOLORATION_OF_FINGERS),
            findViewById<CheckBox>(R.id.BREATH_DIFFICULTY),
            findViewById<CheckBox>(R.id.CHEST_PAIN),
            findViewById<CheckBox>(R.id.LOSS_OF_SPEECH),
            findViewById<CheckBox>(R.id.LOSS_OF_MOVEMENT)
        )
        boxes.map { view ->
            view.setOnClickListener() { _ ->
                selectedOptions[view.tag.toString()] = true
            }
        }
        
        val sendBtn = findViewById<Button>(R.id.send)
        sendBtn.setOnClickListener() { _ ->
            val selected = selectedOptions.keys.filter { k -> selectedOptions[k] == true }.toList()
            val dialog = AlertDialog.Builder(this).create()

            when (selected.size) {
                0 -> {
                    dialog.setTitle("Pelo menos um sintoma precisa ser selecionado")
                    dialog.setMessage("Se você não tem sintomas, não há nada a reportar.")

                    dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Entendi") { dialog, _ ->
                        dialog.dismiss()
                    }
                }
                else -> {
                    dialog.setTitle("Você confirma que possui esses sintomas?")
                    val msg1 = "Essa é uma informação séria. Não crie falsos relatos!"
                    val msg2 = "Voce não poderá reportar novos sintomas por 2 dias."
                    dialog.setMessage("$msg1\n\n$msg2\n")

                    dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar") { dialog, _ ->
                        dialog.dismiss()
                    }

                    dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Confirmar") {dialog, _ ->
                        backend.reportNewSymptoms(deviceId, selected)

                        dialog.dismiss()
                        finish()
                    }
                }
            }

            dialog.show()
        }
    }
}
