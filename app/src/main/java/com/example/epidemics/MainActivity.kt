package com.example.epidemics

import android.Manifest
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.telephony.TelephonyManager
import android.text.Html
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.common.util.concurrent.ListenableFuture
import com.uber.simplestore.executors.StorageExecutors.mainExecutor
import com.uber.simplestore.impl.SimpleStoreFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import java.security.MessageDigest


// Global vars
const val MIN_UPDATE_FREQUENCY = 60
var GLOBAL_LOCATION = Location("gps")
var GLOBAL_CASES_IN_REGION = 0

class MainActivity : AppCompatActivity() {
    var deviceId = ""
    // DISCLAIMER: this is probably not the safest way to do this.
    // 40 chars are enough to avoid collisions?
    private fun getPersonId(imei: String): String {
        return MessageDigest
            .getInstance("SHA-512")
            .digest(imei.toByteArray())
            .fold("", { str, it -> str + "%02x".format(it) })
            .take(40)
    }

    // TODO: add handlers to force people to accept permissions
    private fun assertPermissions() {
        val devicePermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.READ_PHONE_STATE
        )

        when (devicePermission) {
            PackageManager.PERMISSION_GRANTED -> {}
            else -> { ActivityCompat.requestPermissions(
                          this, arrayOf(Manifest.permission.READ_PHONE_STATE), 1) }
        }

        val locationPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        when (locationPermission) {
            PackageManager.PERMISSION_GRANTED -> {}
            else -> { ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1) }
        }

        val internetPermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.INTERNET
        )

        when(internetPermission) {
            PackageManager.PERMISSION_GRANTED -> {}
            else -> { ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.INTERNET), 1) }
        }

        val servicePermission = ContextCompat.checkSelfPermission(
            this,
            Manifest.permission.FOREGROUND_SERVICE
        )

        when(internetPermission) {
            PackageManager.PERMISSION_GRANTED -> {}
            else -> { ActivityCompat.requestPermissions(
                this, arrayOf(Manifest.permission.FOREGROUND_SERVICE), 1) }
        }
    }

    fun assertGPSEnabled() {
        //TODO
    }

    class DisableButtonCallback(val bt: Button) : FutureCallback<String> {
        override fun onSuccess(result: String?) {
            when {
                result.isNullOrEmpty() -> {
                    bt.isEnabled = false
                }
            }
        }

        override fun onFailure(t: Throwable) {
            Log.e(LOG_TAG,"failed to get key from store: ${t.message}")
        }
    }

    class EnableButtonCallback(val bt: Button) : FutureCallback<String> {
        override fun onSuccess(result: String?) {
            when {
                result.isNullOrEmpty() -> {
                    bt.isEnabled = true
                }
            }
        }

        override fun onFailure(t: Throwable) {
            Log.e(LOG_TAG,"failed to get key from store: ${t.message}")
        }
    }

    override fun onResume() {
        super.onResume()
        val store = SimpleStoreFactory.create(this, deviceId)
        Futures.addCallback(
            store.getString("last_symptoms_report_timestamp"),
            EnableButtonCallback(findViewById(R.id.recovery_button)),
            mainExecutor()
        )

        Futures.addCallback(
            store.getString("last_symptoms_report_timestamp"),
            DisableButtonCallback(findViewById(R.id.symptoms_button)),
            mainExecutor()
        )

        Futures.addCallback(
            store.getString("last_symptoms_report_timestamp"),
            DisableButtonCallback(findViewById(R.id.recovery_button)),
            mainExecutor()
        )

        Futures.addCallback(
            store.getString("last_symptoms_report_timestamp"),
            EnableButtonCallback(findViewById(R.id.symptoms_button)),
            mainExecutor()
        )
        store.close()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        assertPermissions()

        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        GLOBAL_LOCATION = locationManager.getLastKnownLocation("gps")

        val telephonyManager = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        deviceId = getPersonId(telephonyManager.getDeviceId())
        val backend = BackendClient(this@MainActivity ,OkHttpClient(), "http://192.168.0.16:51051")
        val store = SimpleStoreFactory.create(this, deviceId)

        // DISPLAY MESSAGES
        val radius = 1
        val bold = "#ffffff"
        val normal = "#bdbdbd"
        val mainMsg = findViewById<TextView>(R.id.main_message)

        val sub1 = findViewById<TextView>(R.id.sub_1)
        sub1.text = "Fique em casa!"

        val sub2 = findViewById<TextView>(R.id.sub_2)
        sub2.text = "Ajude a reduzir a taxa de reprodução do vírus."


        // SETUP BUTTONS
        val recoveryBtn = findViewById<Button>(R.id.recovery_button)
        recoveryBtn.setOnClickListener() {_ ->
            val dialog = AlertDialog.Builder(this).create()
            dialog.setTitle("Você confirma que não apresenta mais sintomas?")
            dialog.setMessage("Essa é uma informação séria. Não crie falsos relatos!")

            dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar") { dialog, _ ->
                dialog.dismiss();
            }

            dialog.setButton(AlertDialog.BUTTON_POSITIVE, "Confirmar") {dialog, _ ->
                backend.reportRecovery(deviceId)
                dialog.dismiss()
            }

            dialog.show()
        }
        Futures.addCallback(
            store.getString("last_symptoms_report_timestamp"),
            DisableButtonCallback(recoveryBtn),
            mainExecutor()
        )
        store.close()

        val symptomsBtn = findViewById<Button>(R.id.symptoms_button)
        symptomsBtn.setOnClickListener() {_ ->
            val intent = Intent(this, SymptomsActivity::class.java)
            intent.putExtra("deviceId", deviceId)
            startActivity(intent)
        }

        // CASES COUNT LOOP
        val updateCases = {
            backend.getCasesWithinRadius(GLOBAL_LOCATION)
            mainMsg.text = Html.fromHtml(
                """
                    <font color="$bold"><big><b>$GLOBAL_CASES_IN_REGION</b></big></font>
                    <font color="$normal">pessoas<br/> com sintomas da Covid-19 no raio de </font>
                    <font color="$bold"><b>${radius}&#8239km </b></font>
                    <font color="$normal"> da sua localização.</font>
                    """.trimIndent()
            )
        }

        GlobalScope.launch {
            repeat(3) {
                updateCases()
            }
            while (true) {
                updateCases()
                delay(30000)
            }
        }

        // BACKGROUND LOCATION SERVICE
        val deviceModel = android.os.Build.MODEL
        val locationIntent = Intent(this, LocationService::class.java)
        locationIntent.putExtra("deviceId", deviceId)
        this.startService(locationIntent)
    }
}



