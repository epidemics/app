package com.example.epidemics

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.IBinder
import android.os.Looper
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.android.gms.location.*
import okhttp3.OkHttpClient

class LocationService : Service() {
    val backend = BackendClient(this@LocationService, OkHttpClient(), "http://192.168.0.16:51051")
    var deviceId: String = ""
    var lastReportedAt: Long = 0
    var lastLocation: Location? = null
    
    private val callback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val location = locationResult.lastLocation

            if (location != lastLocation && nowUnix() - lastReportedAt >= MIN_UPDATE_FREQUENCY) {
                Log.d(LOG_TAG, location.toString())
                backend.reportPosition(location, deviceId)

                lastReportedAt = nowUnix()
                lastLocation = location
                GLOBAL_LOCATION = location
            }
        }
    }

    fun nowUnix(): Long {
        return System.currentTimeMillis() / 1000L
    }
    
    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getChannelId(): String{
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val chan = NotificationChannel(
                "epidemics", "Aplicativo Ativo", NotificationManager.IMPORTANCE_NONE
            )
            
            chan.lightColor = Color.BLUE
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            service.createNotificationChannel(chan)
            return "epidemics"
        } else {
            ""
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        super.onStartCommand(intent, flags, startId)

        val pendingIntent = intent.let { notificationIntent ->
            PendingIntent.getActivity(this, 0, notificationIntent, 0)
        }

        val notification: Notification = Notification.Builder(this, getChannelId())
            .setContentTitle("Fornecendo localização anonimamente")
            .setContentText("Você está ajudando a salvar vidas!")
            .setSmallIcon(R.drawable.ic_launcher_background)
            .setContentIntent(pendingIntent)
            .build()
        
        startForeground(1000, notification)
        
        deviceId = intent!!.extras!!.getString("deviceId")!!

        val req = LocationRequest()
        req.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        req.interval = 2000L
        req.fastestInterval = 1000L

        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(req)
        val locationSettingsRequest = builder.build()

        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        val locationClient = LocationServices.getFusedLocationProviderClient(this)
        locationClient!!.requestLocationUpdates(req, callback, Looper.myLooper())

        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
    }
}
