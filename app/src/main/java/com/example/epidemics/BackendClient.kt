package com.example.epidemics

import android.content.Context
import android.location.Location
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.common.util.concurrent.FutureCallback
import com.google.common.util.concurrent.Futures
import com.google.gson.Gson
import com.uber.simplestore.executors.StorageExecutors.mainExecutor
import com.uber.simplestore.impl.SimpleStoreFactory
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.*
import java.io.IOException
import java.nio.charset.Charset

val MEDIA_TYPE = "application/json".toMediaType()
const val LOG_TAG = "epidemics_app"

fun <T> serialize(datum: T): RequestBody {
    return Gson().toJson(datum).toRequestBody(MEDIA_TYPE)
}

data class PositionObservation(
    val deviceId: String,
    val accuracyMeters: Float,
    val altitudeMeters: Double,
    val longitudeDegrees: Double,
    val latitudeDegrees: Double,
    val speedMetersPerSecond: Float,
    val timestampUnixSeconds: Long
)

data class NewSymptomsReport(
    val deviceId: String,
    val symptoms: List<String>,
    val reportedAtUnixSeconds: Long
)

data class SymptomsRecoveryReport(
    val deviceId: String,
    val reportedAtUnixSeconds: Long
)

data class CountCasesWithinRadiusRequest(
    val longitude: Double,
    val latitude: Double,
    val radiusMeters: Double = 1000.0
)

class ReportNewSymptomsCallback(
    val ctx: Context,
    val deviceId: String
): Callback {
    override fun onFailure(call: Call, e: IOException) {
        Log.e(LOG_TAG, "failed to send position: ${e.toString()}")
        Handler(Looper.getMainLooper()).post {
            val msg = "Houve um erro ao enviar. Verifique a conexão com a internet."
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
        }
    }

    fun nowUnix(): Long {
        return System.currentTimeMillis() / 1000L
    }

    override fun onResponse(call: Call, response: Response) {
        val store = SimpleStoreFactory.create(ctx, deviceId)
        Futures.addCallback(
            store.putString("last_symptoms_report_timestamp", nowUnix().toString()),
            PersistKeyCallback(),
            mainExecutor()
        )
        store.close()

        response.use {
            Handler(Looper.getMainLooper()).post {
                val msg = when {
                    response.isSuccessful -> "Sintomas enviados com sucesso."
                    else -> "Algum erro inesperado ocorreu. Tente novamente mais tarde."
                }

                Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
            }
        }
    }
}

class ReportRecoveryCallback(val ctx: Context, val deviceId: String): Callback {
    override fun onFailure(call: Call, e: IOException) {
        Log.e(LOG_TAG, "failed to send position: ${e.toString()}")
        Handler(Looper.getMainLooper()).post {
            val msg = "Houve um erro ao enviar. Verifique a conexão com a internet."
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
        }
    }

    override fun onResponse(call: Call, response: Response) {
        val store = SimpleStoreFactory.create(ctx, deviceId)
        store.remove("last_symptoms_report_timestamp")
        store.close()

        response.use {
            Handler(Looper.getMainLooper()).post {
                val msg = when {
                    response.isSuccessful -> "Status enviado com sucesso."
                    else -> "Algum erro inesperado ocorreu. Tente novamente mais tarde."
                }

                Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
            }
        }
    }
}

class PersistKeyCallback() : FutureCallback<String> {
    override fun onSuccess(result: String?) {}

    override fun onFailure(t: Throwable) {
        Log.e(LOG_TAG, "failed to persist key: ${t.message}")
    }
}

class CountCasesWithinRadiusCallback(val ctx: Context): Callback {
    data class Resp(val count: Int)

    override fun onFailure(call: Call, e: IOException) {
        Log.e(LOG_TAG, "failed to get local cases: ${e.toString()}")
        Handler(Looper.getMainLooper()).post {
            val msg = "Houve um erro ao buscar casos. Verifique a conexão com a internet."
            Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
        }
    }

    override fun onResponse(call: Call, response: Response) {
        when {
            response.isSuccessful -> {
                val resp = Gson().fromJson(response.body!!.string(), Resp::class.java)
                GLOBAL_CASES_IN_REGION = resp.count

            }
            else -> {
                Handler(Looper.getMainLooper()).post {
                    Log.e(LOG_TAG, "failed to get cases: ${response.code}")
                    val msg = "Algum erro inesperado ocorreu. Tente novamente mais tarde."
                    Toast.makeText(ctx, msg, Toast.LENGTH_LONG).show()
                }
            }
        }
    }
}

class ReportPositionCallback(val ctx: Context): Callback {
    override fun onFailure(call: Call, e: IOException) {
        Log.e(LOG_TAG, "failed to send position: ${e.toString()}")
    }

    override fun onResponse(call: Call, response: Response) {
        response.use {
            if (!response.isSuccessful)
                Log.e(LOG_TAG, "failed to send position: non 2XX response: ${response.code}")
        }
    }
}

class BackendClient(val ctx: Context, val httpClient: OkHttpClient, val host: String) {
    fun reportPosition(location: Location, deviceId: String) {
        val observation = PositionObservation(
            deviceId,
            location.accuracy,
            location.altitude,
            location.longitude,
            location.latitude,
            location.speed,
            location.time / 1000
        )

        val req = Request.Builder()
            .url("$host/api/v1/positions")
            .post(serialize(observation))
            .build()

        httpClient.newCall(req).enqueue(ReportPositionCallback(ctx))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun reportNewSymptoms(deviceId: String, symptoms: List<String>) {
        val report = NewSymptomsReport(
            deviceId,
            symptoms,
            System.currentTimeMillis()/1000L
        )

        val req = Request.Builder()
            .url("$host/api/v1/symptoms")
            .post(serialize(report))
            .build()

        httpClient.newCall(req).enqueue(ReportNewSymptomsCallback(ctx, deviceId))
    }    

    @RequiresApi(Build.VERSION_CODES.O)
    fun reportRecovery(deviceId: String) {
        val report = SymptomsRecoveryReport(
            deviceId,
            System.currentTimeMillis()/1000L
        )

        val req = Request.Builder()
            .url("$host/api/v1/recoveries")
            .post(serialize(report))
            .build()

        httpClient.newCall(req).enqueue(ReportRecoveryCallback(ctx, deviceId))
    }

    fun getCasesWithinRadius(location: Location, radiusMeters: Int = 1000) {
        val creq = CountCasesWithinRadiusRequest(
            location.longitude,
            location.latitude,
            radiusMeters.toDouble()
        )

        val req = Request.Builder()
            .url("$host/api/v1/cases")
            .post(serialize(creq))
            .build()

        httpClient.newCall(req).enqueue(CountCasesWithinRadiusCallback(ctx))
    }
}
