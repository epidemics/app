package com.example.epidemics

import android.location.Location

// val store = Store(State(), ::appReducer)
// val action = Action(App.REPORT_POSITION, location)
// store.dispatch(action)


data class State(
    val lastLocationReport: Int = 0,
    val lastLocation: Location? = null
)

enum class App {
    REPORT_POSITION
}

data class Action<T>(val kind: App, val payload: T)

data class Store(val initialState: State, val reducer: Reducer) {
    private var currentState = initialState
    
    fun dispatch(action: Action<*>) {
        currentState = reducer(currentState, action)
    }

    fun getState(): State {
        return currentState
    }
}

typealias Reducer = (State, Action<*>) -> State

fun appReducer(state: State, action: Action<*>): State {
    return when(action.kind) {
        App.REPORT_POSITION -> state.copy(lastLocation = action.payload as Location)
        else -> state
    }
}

